import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';
import { Persona } from 'src/app/Modelo/Persona';
//import { Serviceservice } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  agregarEmpleadoRegistro: any = 
  {id:'',
   data:{
    nombre:'',
    apellidoPat:'', 
    apellidoMat:'', 
    email:''}}


  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
  }
  Guardar(){
    console.log("evento agregado")

   this.service.agregarEmpleado(this.agregarEmpleadoRegistro).subscribe(resultado =>{
     alert("Se agrego con exito");
    }, 
    error=>{
      console.log(JSON.stringify(error));
    });

  }
}
